#!/usr/bin/env sh

# shellcheck source=helpers/debug-pre-commit-community-health
. "$(dirname "$0")/helpers/debug-pre-commit-community-health"

exit_code=0

git_command=$("$(dirname "$0")/helpers/get-git-command")

echo "Received: $git_command"
echo

case "$git_command" in
  *ci.skip* | \
    *skip.ci*)
    # shellcheck disable=SC2016
    echo 'Using `--push-option=skip.ci` is not allowed in this project'

    exit_code=1
    ;;
esac

exit $exit_code
