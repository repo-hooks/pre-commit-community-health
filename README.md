# Pre-Commit-Community-Health

[![Release][repo-tag-image]][repo-url]
[![License][license-image]][license-url]

A collection of [git] hooks for [pre-commit] to ensure good community health.

## Using Pre-Commit-Community-Health

Add this to your `.pre-commit-config.yaml`

```yaml
- repo: https://gitlab.com/repo-hooks/pre-commit-community-health
  rev: v1.0.0-alpha # Use the ref you want to point at
  hooks:
    - id: block-commit-no-verify
    # - id: ...
```

## Hooks Available

### `block-commit-no-verify`

![prepare-commit-msg hook][hook-prepare-commit-msg-image]

Block a commit that includes the `--no-verify` flag

### `block-push-skip-ci`

![pre-push hook][hook-pre-push-image]

Block a push that includes `ci.skip` or `skip.ci` as a `--push-option`

[git]: https://www.git-scm.org
[hook-prepare-commit-msg-image]: https://img.shields.io/badge/hook-prepare--commit--msg-informational?logo=git
[hook-pre-push-image]: https://img.shields.io/badge/hook-pre--push-informational?logo=git
[license-image]: https://img.shields.io/gitlab/license/repo-hooks/pre-commit-community-health
[license-url]: https://opensource.org/licenses/ISC
[pre-commit]: https://www.pre-commit.com
[repo-tag-image]: https://img.shields.io/gitlab/v/tag/repo-hooks/pre-commit-community-health?include_prereleases&sort=semver
[repo-url]: https://gitlab.com/repo-hooks/pre-commit-community-health
